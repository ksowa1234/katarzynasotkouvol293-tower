﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    [SerializeField] GameObject tower;
    bool rotate = true;
    [SerializeField] GameObject bullet;

    void Start()
    {
        
    }

    void Update()
    {
       if(rotate)
            StartCoroutine(RotateTower());
    }

    IEnumerator RotateTower()
    {

        if (rotate) 
        {
            rotate = false;
            tower.transform.rotation = Quaternion.Euler(new Vector3(tower.transform.rotation.x, tower.transform.rotation.y, tower.transform.rotation.z + Random.RandomRange(-45, 45)));
            Instantiate(bullet, tower.transform);
            Debug.Log("mess");


        }
        yield return new WaitForSeconds(Random.RandomRange(0, 5));
        rotate = true;


    }
}
